﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Agidens.MessageBroker.Msg.PrintServer;
using Newtonsoft.Json;

namespace ReportTest
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            foreach (var knownReport in typeof(KnownReports).GetNestedTypes())
            {
                cbReportName.Items.Add(knownReport.Name);
            }
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (DesignMode)
            {
                return;
            }

            cbLanguage.SelectedIndex = 0;
            cbReportName.SelectedItem = typeof(KnownReports.BoL).Name;
        }
        private void btnWithIdParameter_Click(object sender, EventArgs e)
        {
            var msg = new DocumentPrintRequestMsg
            {
                CultureInfo = cbLanguage.SelectedItem.ToString(),
                ReportParameters = new List<ReportParameter>
                {
                    new ReportParameter
                    {
                        Name = KnownReports.BoL.ParameterNames.WorkOrderEntryId, 
                        Value = textBox1.Text
                    } 
                    
                },
                ReportName = cbReportName.SelectedItem.ToString()//KnownReports.BoL.ReportName
            };

            Refresh(msg);
        }

        private void Refresh(DocumentPrintRequestMsg msg)
        {
            reportViewer1.ReportSource = new Telerik.Reporting.UriReportSource
            {
                Uri = JsonConvert.SerializeObject(msg)
            };

            reportViewer1.RefreshReport();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnWithoutParameter_Click(object sender, EventArgs e)
        {
            var msg = new DocumentPrintRequestMsg
            {
                CultureInfo = cbLanguage.SelectedItem.ToString(),
                ReportName = cbReportName.SelectedItem.ToString(),
            };

            Refresh(msg);
        }
    }
}
