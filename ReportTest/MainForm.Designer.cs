﻿namespace ReportTest
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.UriReportSource uriReportSource1 = new Telerik.Reporting.UriReportSource();
            this.reportViewer1 = new Telerik.ReportViewer.WinForms.ReportViewer();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnWithIdParameter = new System.Windows.Forms.Button();
            this.cbLanguage = new System.Windows.Forms.ComboBox();
            this.btnWithoutParameter = new System.Windows.Forms.Button();
            this.cbReportName = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            this.reportViewer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.reportViewer1.Location = new System.Drawing.Point(0, 77);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ReportEngineConnection = "engine=RestService;uri=http://LT-A60536034.ecorp.int:8080/api/reports;useDefaultC" +
    "redentials=True";
            uriReportSource1.Parameters.Add(new Telerik.Reporting.Parameter("aaa", "aaaa"));
            uriReportSource1.Parameters.Add(new Telerik.Reporting.Parameter("bbb", "bbb"));
            uriReportSource1.Uri = "Report1.trdx";
            this.reportViewer1.ReportSource = uriReportSource1;
            this.reportViewer1.Size = new System.Drawing.Size(745, 346);
            this.reportViewer1.TabIndex = 0;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(163, 13);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 1;
            // 
            // btnWithIdParameter
            // 
            this.btnWithIdParameter.Location = new System.Drawing.Point(270, 13);
            this.btnWithIdParameter.Name = "btnWithIdParameter";
            this.btnWithIdParameter.Size = new System.Drawing.Size(158, 23);
            this.btnWithIdParameter.TabIndex = 2;
            this.btnWithIdParameter.Text = "With Id parameter";
            this.btnWithIdParameter.UseVisualStyleBackColor = true;
            this.btnWithIdParameter.Click += new System.EventHandler(this.btnWithIdParameter_Click);
            // 
            // cbLanguage
            // 
            this.cbLanguage.FormattingEnabled = true;
            this.cbLanguage.Items.AddRange(new object[] {
            "en",
            "nl"});
            this.cbLanguage.Location = new System.Drawing.Point(499, 12);
            this.cbLanguage.Name = "cbLanguage";
            this.cbLanguage.Size = new System.Drawing.Size(56, 21);
            this.cbLanguage.TabIndex = 3;
            this.cbLanguage.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // btnWithoutParameter
            // 
            this.btnWithoutParameter.Location = new System.Drawing.Point(270, 42);
            this.btnWithoutParameter.Name = "btnWithoutParameter";
            this.btnWithoutParameter.Size = new System.Drawing.Size(158, 23);
            this.btnWithoutParameter.TabIndex = 4;
            this.btnWithoutParameter.Text = "Without parameter";
            this.btnWithoutParameter.UseVisualStyleBackColor = true;
            this.btnWithoutParameter.Click += new System.EventHandler(this.btnWithoutParameter_Click);
            // 
            // cbReportName
            // 
            this.cbReportName.FormattingEnabled = true;
            this.cbReportName.Location = new System.Drawing.Point(13, 13);
            this.cbReportName.Name = "cbReportName";
            this.cbReportName.Size = new System.Drawing.Size(144, 21);
            this.cbReportName.TabIndex = 5;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(745, 423);
            this.Controls.Add(this.cbReportName);
            this.Controls.Add(this.btnWithoutParameter);
            this.Controls.Add(this.cbLanguage);
            this.Controls.Add(this.btnWithIdParameter);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.reportViewer1);
            this.Name = "MainForm";
            this.Text = "ReportTest";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.ReportViewer.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnWithIdParameter;
        private System.Windows.Forms.ComboBox cbLanguage;
        private System.Windows.Forms.Button btnWithoutParameter;
        private System.Windows.Forms.ComboBox cbReportName;
    }
}

